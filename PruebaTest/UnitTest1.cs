﻿using System;
using Aseinfo.Pruebas;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaTest
{
    [TestClass]
    public class Caso1Test
    {
        Caso1 caso = new Caso1();

        [TestMethod]
        public void Jugador1Gana()
        {
            Assert.AreEqual("Jugador 1 ganó!", caso.Jugar("roca", "tijeras"));
            Assert.AreEqual("Jugador 1 ganó!", caso.Jugar("tijeras", "papel"));
            Assert.AreEqual("Jugador 1 ganó!", caso.Jugar("papel", "roca"));
        }

        [TestMethod]
        public void Jugador2Gana()
        {
            Assert.AreEqual("Jugador 2 ganó!", caso.Jugar("tijeras", "roca"));
            Assert.AreEqual("Jugador 2 ganó!", caso.Jugar("papel", "tijeras"));
            Assert.AreEqual("Jugador 2 ganó!", caso.Jugar("roca", "papel"));
        }

        [TestMethod]
        public void Empate()
        {
            Assert.AreEqual("Empate!", caso.Jugar("roca", "roca"));
            Assert.AreEqual("Empate!", caso.Jugar("tijeras", "tijeras"));
            Assert.AreEqual("Empate!", caso.Jugar("papel", "papel"));
        }
    }

    [TestClass]
    public class Caso2Test
    {
        [TestMethod]
        public void TestBasicos()
        {
            Assert.AreEqual(false, Caso2.EsCuadrado(-1), "Los numeros negativos no son cuadrados");
            Assert.AreEqual(false, Caso2.EsCuadrado(3), "3 no es un numero cuadrado");
            Assert.AreEqual(true, Caso2.EsCuadrado(4), "4 es un numero cuadrado");
            Assert.AreEqual(true, Caso2.EsCuadrado(25), "25 es un numero cuadrado");
            Assert.AreEqual(false, Caso2.EsCuadrado(26), "26 no es un numero cuadrado");
        }
    }

    [TestClass]
    public class Caso3Test
    {
        [TestMethod]
        public void TestBasicos()
        {
            Assert.AreEqual("0+1+2+3+4+5+6 = 21", Caso3.MostrarSecuencia(6));
            Assert.AreEqual("-15<0", Caso3.MostrarSecuencia(-15));
            Assert.AreEqual("0=0", Caso3.MostrarSecuencia(0));
        }
    }


    [TestClass]
    public class Caso4Test
    {
        [TestMethod]
        public void MalDebeGanar()
        {
            Assert.AreEqual("Resultado de batalla: El mal erradico todo rastro del bien", Caso4.EmpezarBatalla("1 1 1 1 1 1", "1 1 1 1 1 1 1"));
        }

        [TestMethod]
        public void BienDebeGanar()
        {
            Assert.AreEqual("Resultado de batalla: El bien triunfa sobre el mal", Caso4.EmpezarBatalla("0 0 0 0 0 10", "0 1 1 1 1 0 0"));
        }

        [TestMethod]
        public void DebeSerEmpate()
        {
            Assert.AreEqual("Resultado de batalla: No hubo vencedor en esta batalla", Caso4.EmpezarBatalla("1 0 0 0 0 0", "1 0 0 0 0 0 0"));
        }
    }


    [TestClass]
    public class Caso5Test
    {
        [TestMethod]
        public void PruebasBasicas()
        {
            var caso = new Caso5();

            Assert.AreEqual("z", caso.LetrasFaltantes("abcdefghijklmnopqrstuvwxy"));

            Assert.AreEqual("", caso.LetrasFaltantes("abcdefghijklmnopqrstuvwxyz"));

            Assert.AreEqual("zz", caso.LetrasFaltantes("aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyy"));

            Assert.AreEqual("ayzz", caso.LetrasFaltantes("abbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxy"));

            Assert.AreEqual("bcdghjklmpqrtuvwxyz", caso.LetrasFaltantes("aseinfo"));

        }

    }
}
